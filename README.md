# Hacked Mobile

An android application which returns a randomized result stating if you've been hacked or not. It is meant to showcase
how SAST works on Kotlin.

![Screenshot_1](./images/Screenshot_1.png)

## Using on your machine

You can open this in Android Studio and run it on an emulator. I will add more instructions soon.